<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 */

get_header(); ?>

<article class="bloco">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
				<h1><?php _e( 'Not Found', 'twentyfourteen' ); ?></h1>
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
      </div>
    </div>
  </div>
</article>

<?php get_footer(); ?>