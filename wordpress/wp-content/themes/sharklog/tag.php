<?php	get_header(); ?>

<article class="bloco conteudo">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php if ( have_posts() ) : ?>
				<h1 class="tit-small text-center"><?php printf( __( 'Tag Archives: %s', 'twentyfourteen' ), single_tag_title( '', false ) ); ?></h1>

				<div class="row">
					<div class="col-md-8">
						<ul class="lista-garagem">
							<?php while ( have_posts() ) : the_post(); ?>

								<li class="col-sm-6 col-md-4">
									<a href="<?php the_permalink(); ?>">
										<figure>
											<div class="wrap"><?php the_post_thumbnail(); ?></div>
											<figcaption class="lista-info">
												<h3><?php the_field('ano-modelo')?></h3>
												<h2><?php the_title(); ?></h2>
												<!-- <strong>Estoque #<?php the_field('estoque')?></strong> -->
											</figcaption>
										</figure>
									</a>
								</li>

								<?php
									endwhile;
								else :
									// If no content, include the "No posts found" template.
									get_template_part( 'content', 'none' );

								endif;
							?>
						</ul>
					</div>

					<div class="col-md-3 col-md-offset-1">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>