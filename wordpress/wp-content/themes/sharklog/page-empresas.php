<?php 
//	Template Name: Empresas
get_header(); ?>

<article>
	<section class="banner banner-empresa" style="background: url(<?php bloginfo('stylesheet_directory'); ?>/img/nuvens.png); background-size: cover; background-position: top left; background-attachment: fixed;">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/empresas.png">
		<img class="envelope envelope1" src="<?php bloginfo('stylesheet_directory'); ?>/img/envelope1.png">
		<img class="envelope envelope2" src="<?php bloginfo('stylesheet_directory'); ?>/img/envelope2.png">
		<img class="envelope envelope3" src="<?php bloginfo('stylesheet_directory'); ?>/img/envelope3.png">
		<img class="envelope envelope4" src="<?php bloginfo('stylesheet_directory'); ?>/img/envelope4.png">


		<h1 class="tit tit-home"><?php the_title(); ?></h1>
		<p class="lead text-center"><?php the_field('bloco_1_subtitulo'); ?></p>
		<ul class="nav nav-tabs nav-page-servico">
			<li class="active"><a href="#dedicado" data-toggle="tab"><?php the_field('tab_titulo_1'); ?></a></li>
			<li><a href="#express" data-toggle="tab"><?php the_field('tab_titulo_2'); ?></a></li>
		</ul>
	</section>

	<div class="tab-content">
		<div id="dedicado" class="tab-pane fade active in">
			<section id="first" class="bloco">
				<div class="container">
					<div class="row texto">
						<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
							<h2 class="text-center"><?php the_field('bloco_rapidez_titulo'); ?></h2>
							<p class="lead text-center"><?php the_field('bloco_rapidez_subtitulo'); ?></p>
							<div class="shark-menu">
								<img class="finn img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/img/finn.png">
								<ul class="nav nav-tabs nav-rapidez">
									<li class="active"><a href="#1" data-toggle="tab" data-target="#1, #1-2"><?php the_field('aba_1_titulo')?></a></li>
									<li><a href="#2" data-toggle="tab" data-target="#2, #2-2"><?php the_field('aba_2_titulo')?></a></li>
									<li><a href="#3" data-toggle="tab" data-target="#3, #3-2"><?php the_field('aba_3_titulo')?></a></li>
								</ul>
							</div>
						</div>

						<div class="tab-content tab-rapidez">
							<div id="1" class="tab-pane fade active in">
								<div class="col-md-5">
									<div class="rapidez">
										<img src="<?php the_field('rapidez_passo_1_imagem')?>">
									</div>
								</div>
								<div class="col-md-7">
									<h2><?php the_field('rapidez_passo_1_titulo')?></h2>
									<?php the_field('rapidez_passo_1_texto'); ?>
								</div>
							</div>

							<div id="2" class="tab-pane fade">
								<div class="col-md-5">
									<div class="rapidez">
										<img src="<?php the_field('rapidez_passo_2_imagem')?>">
									</div>
								</div>
								<div class="col-md-7">
									<h2><?php the_field('rapidez_passo_2_titulo')?></h2>
									<?php the_field('rapidez_passo_2_texto'); ?>
								</div>
							</div>

							<div id="3" class="tab-pane fade">
								<div class="col-md-5">
									<div class="rapidez">
										<img src="<?php the_field('rapidez_passo_3_imagem')?>">
									</div>
								</div>
								<div class="col-md-7">
									<h2><?php the_field('rapidez_passo_3_titulo')?></h2>
									<?php the_field('rapidez_passo_3_texto'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="bloco bloco-dgn dgn-60">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/motoazul.png">
				<div class="dgn-clip">
					<h2><?php the_field('bloco_1_titulo'); ?></h2>
					<?php the_field('bloco_1_texto'); ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>contato/" class="btn btn-primary btn-filled">Assine o plano Dedicado</a>
				</div>					
			</section>

			<section class="bloco passos">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<h1 class="text-center"><?php the_field('bloco_2_titulo'); ?></h1>
							<p class="text-center lead"><?php the_field('bloco_2_subtitulo'); ?></p>
							<div class="row">
								<div class="col-md-6">
									<div class="item">
										<img src="<?php the_field('passo_1_imagem'); ?>" class="img-responsive">
										<h4><?php the_field('passo_1_titulo'); ?></h4>
										<?php the_field('passo_1_texto'); ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="item">
										<img src="<?php the_field('passo_2_imagem'); ?>" class="img-responsive">
										<h4><?php the_field('passo_2_titulo'); ?></h4>
										<?php the_field('passo_2_texto'); ?>
									</div>
								</div>

								<div class="clearfix"></div>

								<div class="col-md-6 col-md-push-6">
									<div class="item">
										<img src="<?php the_field('passo_3_imagem'); ?>" class="img-responsive">
										<h4><?php the_field('passo_3_titulo'); ?></h4>
										<?php the_field('passo_3_texto'); ?>
									</div>
								</div>

								<div class="col-md-6 col-md-pull-6">
									<div class="item">
										<img src="<?php the_field('passo_4_imagem'); ?>" class="img-responsive">
										<h4><?php the_field('passo_4_titulo'); ?></h4>
										<?php the_field('passo_4_texto'); ?>
									</div>
								</div>
								

								<!-- <div class="col-md-4">
									<div class="item">
										<img src="<?php the_field('passo_5_imagem'); ?>" class="img-responsive">
										<h4><?php the_field('passo_5_titulo'); ?></h4>
										<?php the_field('passo_5_texto'); ?>
									</div>
								</div>

								<div class="col-md-4 col-md-pull-8">
									<div class="item">
										<img src="<?php the_field('passo_6_imagem'); ?>" class="img-responsive">
										<h4><?php the_field('passo_6_titulo'); ?></h4>
										<?php the_field('passo_6_texto'); ?>
									</div>
								</div> -->
						
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="bloco radar" style="background: white url(<?php bloginfo('stylesheet_directory'); ?>/img/pattern_mapa_final.png) center right; background-size: 45% auto; background-repeat: no-repeat;">
				<div class="container">
					<div class="row texto">
						<div class="col-md-6">
							<h2><?php the_field('bloco_3_titulo'); ?></h2>
							<?php the_field('bloco_3_texto'); ?>
						</div>

					</div>
				</div>
			</section>

			<!-- <section class="bloco chamada" style="background: url(<?php the_field('chamada_imagem'); ?>); background-position: top left; background-attachment: fixed;">
				<div class="container">
					<div class="row">
						<div class="blc-white">
							<h3 class="text-center"><?php the_field('chamada_titulo'); ?></h3>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>contato/" class="btn btn-primary btn-filled"><?php the_field('botao_texto'); ?></a>
						</div>
					</div>
				</div>
			</section> -->

		</div>

		<div id="express" class="tab-pane fade">
			<section id="second" class="bloco">
				<div class="container">
					<div class="row texto">
						<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
							<h2 class="text-center"><?php the_field('bloco_rapidez_titulo_2'); ?></h2>
							<p class="lead text-center"><?php the_field('bloco_rapidez_subtitulo_2'); ?></p>
							<div class="shark-menu">
								<img class="finn img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/img/finn.png">
								<ul class="nav nav-tabs nav-rapidez">
									<li class="active"><a href="#12" data-toggle="tab" data-target="#12, #1-2"><?php the_field('aba_1_titulo_2')?></a></li>
									<li><a href="#2" data-toggle="tab" data-target="#22, #2-22"><?php the_field('aba_2_titulo_2')?></a></li>
									<li><a href="#3" data-toggle="tab" data-target="#32, #3-22"><?php the_field('aba_3_titulo_2')?></a></li>
								</ul>
							</div>
						</div>

						<div class="tab-content tab-rapidez">
							<div id="12" class="tab-pane fade active in">
								<div class="col-md-5">
									<div class="rapidez">
										<img src="<?php the_field('rapidez_passo_1_imagem_2')?>">
									</div>
								</div>
								<div class="col-md-7">
									<h2><?php the_field('rapidez_passo_1_2')?></h2>
									<?php the_field('rapidez_passo_1_texto_2'); ?>
								</div>
							</div>

							<div id="22" class="tab-pane fade">
								<div class="col-md-5">
									<div class="rapidez">
										<img src="<?php the_field('rapidez_passo_2_imagem_2')?>">
									</div>
								</div>
								<div class="col-md-7">
									<h2><?php the_field('rapidez_passo_2_titulo_2')?></h2>
									<?php the_field('rapidez_passo_2_texto_2'); ?>
								</div>
							</div>

							<div id="32" class="tab-pane fade">
								<div class="col-md-5">
									<div class="rapidez">
										<img src="<?php the_field('rapidez_passo_3_imagem_2')?>">
									</div>
								</div>
								<div class="col-md-7">
									<h2><?php the_field('rapidez_passo_3_titulo_2')?></h2>
									<?php the_field('rapidez_passo_3_texto_2'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="bloco">
				<div class="container">
					<div class="row texto">
						<div class="col-md-5 col-md-offset-1">
							<h2><?php the_field('bloco_1_titulo_2'); ?></h2>
							<?php the_field('bloco_1_texto_2')?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>contato/" class="btn btn-primary btn-filled">Chame um portador</a>
						</div>
						<div class="col-md-6">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/funcionaria.png">
						</div>
					</div>
				</div>
			</section>

			<section class="bloco empresa-rapidez">
				<div class="container">
					<div class="row texto">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-sm-4">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/img/sua_encomenda_em_boas_maos.png" class="img-responsive">
								</div>
									
								<div class="col-sm-8">
									<h2><?php the_field('bloco_2_titulo_2'); ?></h2>
									<?php the_field('bloco_2_texto_2'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="bloco passos">
				<div class="container">
					<h1 class="text-center"><?php the_field('bloco_passo_titulo_1'); ?></h1>
					<p class="text-center lead"><?php the_field('bloco_passo_texto_1'); ?></p>
					<div class="row texto">
						<div class="col-md-3">
							<div class="item">
								<img src="<?php the_field('passo_1_imagem_2'); ?>" class="img-responsive">
								<h4><?php the_field('passo_1_titulo_2')?></h4>
								<?php the_field('passo_1_texto_2')?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="item">
								<img src="<?php the_field('passo_2_imagem_2'); ?>" class="img-responsive">
								<h4><?php the_field('passo_2_titulo_2')?></h4>
								<?php the_field('passo_2_texto_2')?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="item">
								<img src="<?php the_field('passo_3_imagem_2'); ?>" class="img-responsive">
								<h4><?php the_field('passo_3_titulo_2')?></h4>
								<?php the_field('passo_3_texto_2')?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="item no-arrow">
								<img src="<?php the_field('passo_4_imagem_2'); ?>" class="img-responsive">
								<h4><?php the_field('passo_4_titulo_2')?></h4>
								<?php the_field('passo_4_texto_2')?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- <section class="bloco chamada" style="background: url(<?php the_field('chamada_imagem'); ?>); background-position: top left; background-attachment: fixed;">
				<div class="container">
					<div class="row">
						<div class="blc-white">
							<h3 class="text-center"><?php the_field('chamada_titulo'); ?></h3>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>contato/" class="btn btn-primary btn-filled"><?php the_field('botao_texto'); ?></a>
						</div>
					</div>
				</div>
			</section> -->

		</div>
	</div>
</article>

<?php get_footer(); ?>