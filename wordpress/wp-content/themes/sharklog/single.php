<?php 
get_header(); ?>

<article>
	<?php if(have_posts()): while(have_posts()): the_post();?>
		<section class="bloco">
			<div class="container">
				<div class="row texto">
					<div class="col-md-10 col-md-offset-1">
						<h2><?php the_title(); ?></h2>
						<?php the_content();?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; endif?>

</article>

<?php get_footer(); ?>