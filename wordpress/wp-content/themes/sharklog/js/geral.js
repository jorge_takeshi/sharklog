$(function(){ 

	var windowScroll = $(window).scrollTop();
	var body = $('body');
	var banner = $('.banner');
	var bannerHeight = $('.banner').height();
	var banner2 = bannerHeight/4;

	var bannerEcomerce = $('.banner-ecomerce');

	if(banner){

		window.onresize = function () {
			bannerHeight = $('.banner').height();
		}

		checkBannerHeight ();

		window.onscroll = function () {
			windowScroll = $(window).scrollTop();

			checkBannerHeight ();

			if(windowScroll > banner2) {
				$('.motoqueiro').addClass('scroll');
			} else {
				$('.motoqueiro').removeClass('scroll');
			}
		}
	}

	function checkBannerHeight () {
		if(windowScroll + 150 < bannerHeight) {
			$(body).addClass('glass');
		} else {
			$(body).removeClass('glass');
		}
	}

	if(bannerEcomerce) {
		var astro = $('#astronauta');
		var mug = $('#mug');
		var doll = $('#doll');
		var book = $('#book');
		var shirt = $('#shirt');
		var note = $('#note');

		var astroTop = parseInt($(astro).css("top"));
		var astroLeft = parseInt($(astro).css("left"));

		var mugTop = parseInt($(mug).css("top"));
		var mugLeft = parseInt($(mug).css("left"));

		var dollTop = parseInt($(doll).css("top"));
		var dollLeft = parseInt($(doll).css("left"));

		var bookTop = parseInt($(book).css("top"));
		var bookLeft = parseInt($(book).css("left"));

		var shirtTop = parseInt($(shirt).css("top"));
		var shirtLeft = parseInt($(shirt).css("left"));

		var noteTop = parseInt($(note).css("top"));
		var noteLeft = parseInt($(note).css("left"));

		$(window).on('scroll', function(){
			objectBanner(astro, astroTop, 0, -1.5, 1.5, 0);
			objectBanner(mug, mugTop, mugLeft, 0.2, 0, 0.6);
			objectBanner(doll, dollTop, dollLeft, 0.7, 0, -0.8);
			objectBanner(book, bookTop , bookLeft, 0.3, 0, 0.5);
			objectBanner(shirt, shirtTop, shirtLeft, 0.4, 0, -0.2);
			objectBanner(note, noteTop, noteLeft, 0.4, 0, -0.2);
		});
	}

	function objectBanner (x, topPos, leftPos, xspeed, yspeed, rotation) {
		$(x).css({'top': topPos + windowScroll * xspeed, 'left': leftPos + windowScroll * yspeed, transform: 'rotate(' + windowScroll * rotation + 'deg)' });
	}

	var sharkMenu = $('.shark-menu');

	if(sharkMenu) {
		var lastPos1 = 0;
		var lastPos2 = 0;
		var dedicado = true;
		var menuWidth = $(sharkMenu).width();

		function moveFinn (btn) {
			var btnPos = $(btn).parent().position().left;
			var finn = $(btn).parents('.shark-menu').find('.finn');
			
			window.onresize = function() {
				menuWidth = $(sharkMenu).width();
			}

			if( $(btn).parent().is(":first-child") ) {
				$(finn).css('left', 16);
			}
			else if( $(btn).parent().is(":last-child") ) {
				var local = $(sharkMenu).width();
				$(finn).css('left', menuWidth - 115 + "px");
				$(finn).removeClass('reverse');
			} else {
				var local = btnPos + $(btn).parent().width()/2;
				$(finn).css('left', local - 45 + "px");
			}
			reverse (btn, finn);
		}

		function reverse (btn, finn) {
			if(dedicado == true) {
				if( $(btn).position().left < lastPos1) {
					$(finn).addClass('reverse');
				} else {
					$(finn).removeClass('reverse');
				}
				lastPos1 = $(btn).position().left;
			} else {
				if( $(btn).position().left < lastPos2) {
					$(finn).addClass('reverse');
				} else {
					$(finn).removeClass('reverse');
				}
				lastPos2 = $(btn).position().left;
			}
		}

		$('#dedicado .shark-menu li a').on('click', function(){
			dedicado = true;
			moveFinn($(this));
		});

		$('#express .shark-menu li a').on('click', function(){
			dedicado = false;
			moveFinn($(this));
		});

	}

	var scroller = $('.scroller');

	$(scroller).on('click', function(){
		var thisId = $(this).attr('href');

		$('html, body').animate({ scrollTop: $(thisId).offset().top }, 1000);
		return false;
	});

	var dropdown = $('.dropdown-link');
	var toggleMenu = $('.navbar-toggle');
	var open = false;

	$(dropdown).on('click', function(){
		if(open == false) {
			$(body).addClass('opening').delay(300).queue(function(next){
				$(body).removeClass('opening').addClass('open').dequeue();
				open = true;
			});
		}
		else {
			$(body).addClass('opening').delay(300).queue(function(next){
				$(body).removeClass('open').removeClass('opening').dequeue();
				open = false;
			});
		}
		
		return false;
	});

	$(toggleMenu).on('click', function(){
		$(body).addClass('opening').delay(300).queue(function(next){
			$(body).removeClass('open').removeClass('opening').dequeue();
			open = false;
		});
	});


});	