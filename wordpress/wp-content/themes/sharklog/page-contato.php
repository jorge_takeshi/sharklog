<?php 
//	Template Name: Contato

get_header(); ?>

<article>
	<section class="header bloco">
		<div class="container">
			<div class="row texto">
				<div class="col-md-8 col-md-offset-2">
					<div class="form-contato">
						<?php if(have_posts() ): while(have_posts()): the_post();
							the_content(); 
							endwhile; endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</article>

<?php get_footer(); ?>