<?php 
//	Template Name: Dúvidas

get_header(); ?>

<header class="header bloco text-center">
   <h1><?php the_title(); ?></h1>
   <p class="text-center lead">Perguntas frequentes</p>
</header>

<article class="bloco conteudo branco faq">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="branco">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          	<?php
              $contador == 0;
              if( have_rows('faq') ):
                while ( have_rows('faq') ) : the_row();
                $contador++;
              ?>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?php echo $contador; ?>">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $contador; ?>" aria-expanded="false" aria-controls="collapse<?php echo $contador; ?>">
                      <?php the_sub_field('pergunta'); ?>
                      <i class="fa fa-chevron-right"></i>
                    </a>
                  </h4>
                </div>
                <div id="collapse<?php echo $contador; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $contador; ?>">
                  <div class="panel-body">
                    <?php the_sub_field('resposta'); ?>
                  </div>
                </div>
              </div>
            <?php
                endwhile;
              endif;
            ?> 
          </div>
        </div>
      </div>
    </div>
  </div>
</article>

<?php get_footer(); ?>