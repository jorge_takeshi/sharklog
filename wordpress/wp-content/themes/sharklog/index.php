<?php get_header(); ?>

<header class="header bloco">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Blog</h1>
				<p class="lead text-center">Leia nossos posts</p>
			</div>
		</div>
	</div>
</header>

<div class="container">
	<div class="row">
<?php $counter = 0; if(have_posts()): while(have_posts()): the_post(); $counter++; ?>
	<article class="col-md-4 post">
		<figure>
			<?php if(get_the_post_thumbnail()): ?>
				<a class="img-wrap" href="<?php the_permalink(); ?>">
			 		<?php the_post_thumbnail(); ?>
			 	</a>
			 <?php else: ?>
				<a class="img-wrap" href="<?php the_permalink(); ?>">
					<img src="http://placehold.it/330x200" class="img-responsive">
				</a>
			<?php endif?>
			<figcaption>
				<a href="<?php the_permalink(); ?>"><h3><?php echo the_title(); ?></h3></a>
				<?php the_excerpt() ;?>
				<a href="<?php the_permalink(); ?>">Leia mais</a>
			</figcaption>
		</figure>
	</article>
	<?php if($counter%3 == 0): ?>
		<div class="clearfix"></div>
		
<?php endif; endwhile; else: ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif;?>
</div>
</div>

<?php get_footer(); ?>