<?php 
//	Template Name: Homepage

get_header(); ?>

<?php while(have_posts()) : the_post();
$thumb_id = get_post_thumbnail_id();
$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); ?>
<div class="banner banner-home">
	<h1 class="tit tit-home"><?php the_field('banner_subtitulo'); ?></h1>

	<div class="motoqueiro">
		<div class="moto"></div>
		<div class="roda roda1"></div>
		<div class="roda roda2"></div>
		<div class="fumaca"></div>
	</div>
</div>
<?php endwhile; ?>
<article>
	<section id="dedicado" class="bloco">
		<div class="container">
			<div class="row texto">
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
					<h2 class="text-center"><?php the_field('bloco_1_titulo'); ?></h2>
					<p class="lead text-center"><?php the_field('bloco_1_subtitulo'); ?></p>
					<div class="shark-menu">
						<img class="finn img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/img/finn.png">
						<ul class="nav nav-tabs nav-rapidez">
							<li class="active"><a href="#1" data-toggle="tab" data-target="#1, #1-2"><?php the_field('aba_1_titulo')?></a></li>
							<li><a href="#2" data-toggle="tab" data-target="#2, #2-2"><?php the_field('aba_2_titulo')?></a></li>
							<li><a href="#3" data-toggle="tab" data-target="#3, #3-2"><?php the_field('aba_3_titulo')?></a></li>
						</ul>
					</div>
				</div>

				
				<div class="tab-content tab-rapidez">
					<div id="1" class="tab-pane fade active in">
						<div class="col-md-6">
							<div class="rapidez">
								<img src="<?php the_field('bloco_1_imagem_passo_1'); ?>">
							</div>
						</div>

						<div class="col-md-6">
							<div class="tab-content tab-rapidez" role="tab">
								<h2><?php the_field('bloco_1_titulo_passo_1')?></h2>
								<?php the_field('bloco_1_texto_passo_1'); ?>
							</div>
						</div>
					</div>

					<div id="2" class="tab-pane fade">
						<div class="col-md-6">
							<div class="rapidez">
								<img src="<?php the_field('bloco_1_imagem_passo_2'); ?>">
							</div>
						</div>

						<div class="col-md-6">
							<div class="tab-content tab-rapidez" role="tab">
								<h2><?php the_field('bloco_1_titulo_passo_2')?></h2>
								<?php the_field('bloco_1_texto_passo_2'); ?>
							</div>
						</div>
					</div>

					<div id="3" class="tab-pane fade">
						<div class="col-md-6">
							<div class="rapidez">
								<img src="<?php the_field('bloco_1_imagem_passo_3'); ?>">
							</div>
						</div>

						<div class="col-md-6">
							<div class="tab-content tab-rapidez" role="tab">
								<h2><?php the_field('bloco_1_titulo_passo_3')?></h2>
								<?php the_field('bloco_1_texto_passo_3'); ?>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="bloco">
		<div class="container">
			<div class="row texto">
				<div class="col-md-5 col-md-offset-1">
					<h2><?php the_field('bloco_2_titulo'); ?></h2>
					<?php the_field('bloco_2_texto'); ?>
					<a class="btn btn-primary btn-filled">Comece agora</a>
				</div>
				<div class="col-md-6">
					<img src="<?php the_field('bloco_2_imagem'); ?>">
				</div>
			</div>
		</div>
	</section>

	<section class="bloco bloco-dgn" style="background: #eee url('<?php the_field('bloco_3_imagem'); ?>') top left repeat; background-attachment: fixed">
		<div class="dgn-content">
			<h2><?php the_field('bloco_3_titulo'); ?></h2>
			<?php the_field('bloco_3_texto'); ?>
			<a class="btn btn-primary btn-filled">Comece agora</a>
		</div>
	</section>

	<section class="bloco acompanhamento">
		<div class="container-fluid">
			<div class="row texto">
				<div class="col-sm-4 col-sm-offset-2">
					<h2><?php the_field('bloco_4_titulo'); ?></h2>
					<?php the_field('bloco_4_texto'); ?>
				</div>
				<div class="col-sm-6">
					<img src="<?php the_field('bloco_4_imagem') ?>" class="img-responsive"/>
				</div>
			</div>
		</div>
	</section>

	<section class="bloco">
		<div class="container">
			<div class="row servicos">

				<div class="col-md-4">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/ecommerce-circulo.png" class="img-responsive"/>
					<h4><?php the_field('coluna_1_titulo'); ?></h4>
					<?php the_field('coluna_1_texto'); ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>e-commerce/">Veja mais</a>
				</div>
				<div class="col-md-4">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/empresas-circulo.png" class="img-responsive"/>
					<h4><?php the_field('coluna_2_titulo'); ?></h4>
					<?php the_field('coluna_2_texto'); ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>empresas/">Veja mais</a>
				</div>
				<div class="col-md-4">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/lab-circulo.png" class="img-responsive"/>
					<h4><?php the_field('coluna_3_titulo'); ?></h4>
					<?php the_field('coluna_3_texto'); ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>laboratorios/">Veja mais</a>
				</div>
				
			</div>
		</div>
	</section>

</article>

<?php get_footer(); ?>