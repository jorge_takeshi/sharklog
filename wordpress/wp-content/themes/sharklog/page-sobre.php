<?php
//	Template Name: Quem Somos
get_header(); ?>

<article>
	<?php
		while ( have_posts() ) : the_post();
	?>
	<div class="banner-md">
  		<?php the_post_thumbnail('full'); ?>
  	</div>

  <div class="conteudo">
		<div class="bloco">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
            <header class="text-center">
              <h1 class="tit-principal"><span>The Garage</span></h1>
              <span class="risco"></span>
              <p class="text-center">O melhor consignador de carros antigos do Brasil</p>
            </header>

						<p>A The Garage nasceu da minha paixão por carros antigos e de uma decisão pessoal por criar um conceito diferenciado para facilitar a vida de quem quer comprar e vender.</p>

						<p>Minha história com carros antigos tornou-se intensa quando tirei um ano sabático nos USA, em 2001, e me deparei com uma infinidade de carros clássicos, cada um mais lindo que o outro, espalhados por todos os lugares, especialmente em San Diego. Neste lugar eu tive a oportunidade de ter um contato mais próximo com as relíquias do antigomobilismo e conhecer bem o perfil das pessoas desse meio.</p>

						<p>A partir dessa viagem, que me rendeu uma vida dedicada à especialização em carros antigos, passei a me aprofundar no estudo dessas máquinas, a conhecer pessoas influentes nesta área, oferecer suporte aos proprietários de carros clássicos que querem vender seu veículo recebendo um atendimento diferenciado e ajudar aqueles que procuram carros específicos a encontrar o que desejam.</p>

						<p>Assim, a The Garage é uma empresa que fala a língua de quem é amante de carros antigos e ajuda quem quer comprar e quem quer vender, tendo como diferencial um atendimento personalizado feito por quem tem uma larga experiência neste ramo.</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</article>

<article>
  <div class="conteudo">
    <div class="bloco branco">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <header class="text-center">
                  <h1 class="tit-principal"><span>Serviços</span></h1>
                   <span class="risco"></span>
                  <p class="text-center">O melhor consignador de carros antigos do Brasil</p>
                </header>

            <p>A The Garage está preparada para ajudar você a adquirir ou vender o seu carro clássico. Com anos de experiência no ramo, somos a melhor consignadora do país com qualidade e excelência em tudo o que fazemos.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="bloco">
    <div class="container conteudo">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-xs-12">
              <h2>Consignação</h2>
              <p>Nosso serviço de consignação é inovador e extremamente vantajoso para quem deseja vender seu carro antigo. O veículo é apresentado aos interessados em nossa loja virtual e em diversos outros sites do ramo, de maneira a ser facilmente encontrado por quem tem interesse em comprá-lo.</p>

              <p>Nós tratamos de toda negociação e documentação, para que você não precise resolver questões burocráticas, tendo o conforto de vender seu carro sem qualquer tipo de preocupação.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="bloco branco">
    <div class="container conteudo">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-xs-12">
              <h2>Compra</h2>
              <p>Disponibilizamos um vasto acervo de carros antigos em nossa loja virtual, atendendo aos mais variados estilos. Por este motivo, é muito provável que você encontre exatamente o que procura ao navegar em nossas páginas.</p>

              <p>Nossa especialidade é vender carros clássicos, sendo atualmente a melhor loja virtual deste ramo no mercado. Este é o lugar onde você entrará como um apreciador de veículos antigos e sairá como proprietário da relíquia que havia idealizado.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="bloco">
    <div class="container conteudo">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-xs-12">
              <h2>Importação</h2>
              <p>Se você precisa de ajuda para importar o seu carro clássico, nós fornecemos suporte completo, auxiliando-o na procura pelo veículo desejado, negociação, vistoria, documentação e envio para o Brasil.</p>

              <p>Além disso, se você quiser mandar o seu carro clássico para outro país, também disponibilizamos assessoria completa e tratamos de todo o passo-a-passo para o envio, oferecendo suporte até a chegada do veículo em seu destino e finalização do processo.</p>

              <p>São mais de dez anos de experiência no ramo de importação de carros antigos, proporcionando conforto, segurança e realização aos amantes dos clássicos. Fazemos o serviço porta-a-porta, sem burocracia, tornando a importação do seu carro um acontecimento agradável e especial.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
      endwhile;
    ?>
  </div>
</article>

<article class="bloco branco faq">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <header class="text-center">
           <h1 class="tit-principal"><span>FAQ</span></h1>
           <span class="risco"></span>
           <p class="text-center">Perguntas e respostas frequentes de nossos clientes</p>
        </header>

        <div class="branco">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php
              $contador == 0;
              if( have_rows('faq') ):
                while ( have_rows('faq') ) : the_row();
                $contador++;
              ?>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?php echo $contador; ?>">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $contador; ?>" aria-expanded="false" aria-controls="collapse<?php echo $contador; ?>">
                      <?php the_sub_field('pergunta'); ?>
                    </a>
                  </h4>
                </div>
                <div id="collapse<?php echo $contador; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $contador; ?>">
                  <div class="panel-body">
                    <?php the_sub_field('resposta'); ?>
                  </div>
                </div>
              </div>
            <?php
                endwhile;
              endif;
            ?> 
          </div>
        </div>
      </div>
    </div>
  </div>
</article>

<?php get_footer(); ?>