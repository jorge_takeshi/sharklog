<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title><?php wp_title( '|', true, 'right' ); ?></title>

		<link href="<?php bloginfo('stylesheet_directory') ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php bloginfo('stylesheet_directory') ?>/css/style.css" rel="stylesheet">
		<link href="<?php bloginfo('stylesheet_directory') ?>/slick/slick.css" rel="stylesheet" type="text/css"/>
		<link href="<?php bloginfo('stylesheet_directory') ?>/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700|Ubuntu:300,400,700" rel="stylesheet">
	
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '616746935143007');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=616746935143007&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

		<?php wp_head(); ?>
	</head>
	
	<body class="glass" <?php body_class(); ?>>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('stylesheet_directory') ?>/img/logo.png">
					</a>

					<div class="div-toggle">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-control="navbar"><i class="fa fa-bars"></i></button>
					</div>
				</div>
				<div class="navbar-collapse collapse" id="navbar" aria-expanded="true">
					<ul class="nav-menu nav navbar-nav pull-right">
						<ul id="menu-menu-topo" class="menu">
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Sharklog</a></li>
							<li><a class="dropdown-link" href="#">Serviços</a>
								<ul class="menu-dropdown">
									<li>
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>empresas/">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/img/empresas-circulo.png">
											<strong>Empresas</strong>
											<p>Com portadores bem treinados a entrega é mais ágil pra você.</p>
										</a>
									</li>
									<li>
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>laboratorios/">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/img/lab-circulo.png">
											<strong>Laboratórios</strong>
											<p>Nós garantimos uma entrega rápida, cuidadosa e segura.</p>
										</a>
									</li>
									<li>
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>e-commerce/">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/img/ecommerce-circulo.png">
											<strong>E-commerce</strong>
											<p>Para você ter tempo livre para se dedicar ao seu negócio.</p>
										</a>
									</li>
								</ul>
							</li>
							<li><p class="navbar-text">Solicitação: (11) 2901-9830</p></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contato/">Atendimento</a></li>
						</ul>
						<?php //wp_nav_menu( array( 'menu' => 'Menu Topo', 'container' => ' ', 'walker' => new Menu_The_Garage() ) ); ?>
					</ul>
				</div>
			</div><!-- /.container-fluid -->
		</nav>
