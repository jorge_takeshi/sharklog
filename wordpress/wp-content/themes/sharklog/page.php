<?php get_header(); ?>

<article>
	<?php
		while ( have_posts() ) : the_post();
	?>
	<div class="banner-md">
  		<?php the_post_thumbnail('full'); ?>
  	</div>

	 <div class="conteudo">
		<?php the_content(); ?>

		<?php
			endwhile;
		?>
	</div>
</article>

<?php get_footer(); ?>