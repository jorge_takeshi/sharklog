<?php

// Nav Walker

Class Menu_The_Garage extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		if($depth == 0) {
			$output .= "\n<ul class='menu-dropdown'>\n";
		} 
		else {
			$output .= "\n<ul>\n";
		}
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "\n</ul>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id=0 ) {

		if( $item ->url == "#" ) {
			$output .= "\n<li><a class='dropdown-link' href='#'>".$item->title."</a>";
		} else {
			$output .= "\n<li><a href=".esc_attr($item->url).">".$item->title;
		}
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</a></li>\n";
	}
}

// Registra excerpt nas páginas
add_action( 'init', 'my_add_excerpts_to_pages' );
	function my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}


// Retira o "Leia mais" no fim dos posts
function sbt_auto_excerpt_more( $more ) {
return 'aaa';
}
add_filter( 'excerpt_more', 'sbt_auto_excerpt_more', 20 );

function sbt_custom_excerpt_more( $output ) {return preg_replace('/<a[^>]+>Continue reading.*?<\/a>/i','',$output);
}
add_filter( 'get_the_excerpt', 'sbt_custom_excerpt_more', 20 );

// Retira protected e private dos títulos dos posts
function protect_my_privates($text){
  $text='%s';
  return $text;
}
add_filter('private_title_format','protect_my_privates');
add_filter('protected_title_format', 'protect_my_privates');

// Classe img-responsive nos thumbs
add_filter('post_thumbnail_html','add_class_to_thumbnail');
		function add_class_to_thumbnail($thumb) {
		$thumb = str_replace('attachment-', 'img-responsive attachment-', $thumb);
		return $thumb;
}

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
  $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
  return $html;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


// Define a largura da área de conteúdo
	if ( ! isset( $content_width ) )
   		$content_width = 980;

function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

	// Define a largura da área de conteúdo
	if ( ! isset( $content_width ) )
   		$content_width = 680;
	
		// Define a largura da área de conteúdo
		add_image_size( 'carro-thumb', 360, 9999 ); //360 pixels wide (and unlimited height)

		add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );
	}
?>