
		<footer class="bloco secundario">
			<?php dynamic_sidebar('sidebar-2'); ?>
			<div class="container-fluid">
				<div class="row">
					<nav class="col-md-4">
						<ul class="footer-categorias">
							<li><h4>Navegue</h4></li>
							<?php 
							wp_nav_menu(array('menu'=>'Menu Rodapé'));
							?>
						</ul>
					</nav>

					<section class="col-md-4">
						<div class="row">
						<div class="footer-contato">
							<h4>Entre em contato 11. 2901-9830</h4>
							<!--<span class="tel">11. 2901-9830</span>
							<span class="tel">11. 96591-5589</span> -->
							<span class="atendimento">Atendimento de segunda a sexta das 9h00 as 18h00</span>
							<!-- <span class="address">Av. Comendador Alberto Bonfiglioli, 131 - Granja Viana - Cotia - SP</span> -->
						</div>
					</div>
					</section>

					<section class="col-md-3 col-md-offset-1">
						<ul class="footer-social">
							<li><a href="https://www.facebook.com/thegaragebr/" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://www.instagram.com/thegarage_br/" target="_blank"><i class="fa fa-instagram"></i></a></li>
							<li><a href="https://www.twitter.com/thegarage_br/" target="_blank"><i class="fa fa-twitter"></i></a></li>
						</ul>
					</section>
				</div>
				<div class="line row">
					<div class="col-md-12">
						<span>Existe um caminho rápido e seguro para entregas <img class="logo-footer" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo_branco.png"></span>
					</div>
				</div>
			</div>
			
		</footer>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?php bloginfo('stylesheet_directory') ?>/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/slick/slick.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/slider.js"></script>
		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/geral.js"></script>
		
		<!-- Google Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-72918261-1', 'auto');
		  ga('send', 'pageview');

		</script>

		<!-- RD Station -->
		<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/e37894b2-532d-48a9-8965-c24f01843c42-loader.js" ></script>

		<?php wp_footer(); ?>
	</body>
</html>